import { Component } from '@angular/core';
var UsersComponent = /** @class */ (function () {
    function UsersComponent() {
    }
    UsersComponent.decorators = [
        { type: Component, args: [{
                    selector: 'user-comp',
                    templateUrl: './users.component.html',
                    styleUrls: ['./users.component.css']
                },] },
    ];
    /** @nocollapse */
    UsersComponent.ctorParameters = function () { return []; };
    return UsersComponent;
}());
export { UsersComponent };
