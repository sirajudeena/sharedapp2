import { Component } from '@angular/core';

@Component({
    selector: 'user-comp',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.css']
})
export class UsersComponent {}